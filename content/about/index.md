---
title: About page
subtitle: Software Developer
comments: false
---

Check that site params can be extracted and displayed here... \
{{< contact_name >}} \
{{< contact_email >}} \
{{< contact_phone >}}

... as in this screenshot: \
![this](about.png)

If no site params are shown, check
[this]({{< ref "/posts/2025-02-05-test" >}})
post.
