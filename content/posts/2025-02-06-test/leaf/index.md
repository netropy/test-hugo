---
title: '6. /content/posts/2025-02-06-test/leaf/index'
date: 2025-02-06
params:
  subtitle: "Check that this file is NOT rendered as a page"
categories: ["web development"]
tags: ["hugo", "hugo page bundle"]
---

Given the file `../index.md`, this file's parent directory is a _leaf bundle,_
which cannot have any descendants.

If this file's content is rendered as a page, try: delete `/public`, clear
browser's history, restart Hugo.
