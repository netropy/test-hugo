---
title: "6. Check files '_index' and 'index'"
date: 2025-02-06
params:
  subtitle: "Check the effect of content directory files '_index' and 'index'"
summary: "File `_index` makes a branch bundle, file `index` a leaf bundle in the page hierarchy."
description: no description.
categories: ["web development"]
tags: ["hugo", "hugo page bundle"]
---

#### Check for content and rendering:

- this site's [home](/) page reflects file `/content/_index.md`
- the section page [posts](./index) reflects file `/content/posts/_index.md`
  (and not `index.md`)
- the [about](/about) page reflects file `/content/about/index.md`
- this page reflects file `/content/posts/2025-02-06-test/index.md`
- this leaf bundle's link [./leaf/](./leaf/) results some 'Page Not Found'
  error

### The role of special file: `_index.md`

A file
[\_index.md](https://gohugo.io/content-management/organization/#index-pages-index-md)
allows to add content (text, images etc.) and
[front matter](https://gohugo.io/content-management/front-matter/)
(metadata) to: \
\- the site's _home page_ as file `/content/_index.md` \
\- [sections](https://gohugo.io/content-management/sections/) (see below)
   in the `/content/` directory hierarchy. \
\- [taxonomies](https://gohugo.io/content-management/taxonomies/)
   (groupings of content) and _term pages_ (keys in a taxonomy), see below\*.

A content directory is treated as a _section_ if \
\- it has an `./_index` file, \
\- or is a top-level directory under `/content/`

_Sections_ behave differently from non-section content directories:
- A section's directory name becomes an _URL segment,_ for example: \
  `posts` in `{{% page_section_url %}}`
- Relative paths like `xyz` or `./xyz` in markdown links are based off the
  current directory (section), not the parent section.
- So, paths `./index` and `./_index` link to the landing page of the current
  section (directory).
- Hugo processes a section as a
  [list page](https://gohugo.io/templates/lookup-order/#kind),
  generating a listing of all pages in the section (that don't belong to
  subsections).
- Section directories are also called
  [branch bundles](https://gohugo.io/quick-reference/glossary/#branch-bundle).

__\* Problem, 2025-02: Unclear Hugo documentation:__
> Could not find an authoritative code example: For _taxonomy_ and _term_
> pages, where go `_index.md` files, under `/content/` or `/layouts/`? \
> Guess: under `/content/`, with `/layouts/` only hosting `.html` templates.

### The role of special file: `index.md`

The file `index.md/html`:
- serves as the _landing page_ of a directory (by convention of web servers);
- can have
  [front matter](https://gohugo.io/content-management/front-matter/)
  and content;
- is processed by Hugo as a
  [single page](https://gohugo.io/templates/lookup-order/#kind)
  (regular content page);
- cuts off any subdirectories from being rendered as web pages.

Directories with an `index.md` file are called
[leaf bundles](https://gohugo.io/quick-reference/glossary/#leaf-bundle). \
Together branch bundles and leaf bundles form
[page bundles](https://gohugo.io/content-management/page-bundles/).

#### If a directory has both an `_index` and `index` file:

See `/content/posts/`: the file `_index.md` masks file `index.md`. \
Hugo processes all content files, but then file `_index.md` takes precedence.
