---
title: 'File /content/posts/_index.md'
date: 2025-02-06
params:
  subtitle: "Check that the section page 'Posts' reflects this file"
---

As a top-level directory under `/content/`, `posts/` is automatically a
[section](https://gohugo.io/content-management/sections/);
therefore, the file `_index.md` is not needed for this purpose.

But then, `_index.md` can also provide some content to be displayed with the
generated list of pages in this section, as shown here.
