---
title: 'File /content/posts/index.md'
date: 2025-02-06
params:
  subtitle: "Check that this file is NOT rendered as a page"
---

This file `index.md` should be masked by file `_index.md` in same directory.

If this file's content is rendered as a page, try: delete `/public`, clear
browser's history, restart Hugo.
