---
title: "3. Check site-local markdown links"
date: 2025-02-03
params:
  subtitle: Check that local page and image links work
description: no description.
categories: ["web development", "blogging"]
tags: ["hugo", "hugo markdown links", "hugo url management", "hugo content management"]
---

Summary: Prefer _relative_ over absolute markdown links. \
No need for use of shortcodes `ref` or `relref`. \
Instead, enable _link_ and _image render hooks._ \
Place global resources under `assets/`.
<!--more-->

### Check cross-referencing of site-local pages

Page _2025-02-03-test_ (`index.md`) links to _page_ (`../page.md`) with this
directory layout:
```
my-project/
+--+ content/
     +-- posts/
         +-- page.md
         +-- 2025-02-03-test
             +-- index.md
```

#### Click on or hover over the _page_ links and check against _works_ (+/-):

| path | markdown link | rendered | works | need_hook | comment |
|:--|:--|:--:|:--:|:--:|:--|
| relative | `[page](page)` | [page](page) | + | + | __<-- BEST__ |
| relative | `[page](page/)` | [page](page/) | + | + | same, stripped "/" |
| relative | `[page](../../posts/page)` | [page](../../posts/page) | + | + | can navigate, content/ structure is preserved |
| relative | `[page](../../../content/posts/page)` | [page](../../../content/posts/page) | - | + | content/ is a source dir, not a build dir |
| absolute | `[page](/posts/page)` | [page](/posts/page) | + | + | absolut path is relative to content dir |
| absolute | `[page](/page/)` | [page](/page/) | - | + | absolut path is not relative to local dir |
| relative | `[page]({{%/* ref "page" */%}})` | [page]({{% ref "page" %}}) | + | - | convoluted |
| absolute | `[page]({{%/* ref "/posts/page" */%}})` | [page]({{% ref "/posts/page" %}}) | + | - | convoluted |
| relative | `[page]({{%/* relref "page" */%}})` | [page]({{% relref "page" %}}) | + | - | convoluted |
| absolute | `[page]({{%/* relref "/posts/page" */%}})` | [page]({{% relref "/posts/page" %}}) | + | - | convoluted |

#### HowTo: Enable the _link render hook_

For Markdown _page links_ to work without the shortcodes
[Ref](https://gohugo.io/shortcodes/ref/) or
[RelRef](https://gohugo.io/shortcodes/relref/),
enable the embedded
[link render hook](https://gohugo.io/render-hooks/links/)
in the site configuration (or provide custom hooks):
```
  markup:
    goldmark:
      renderHooks:
        link:
          enableDefault: true  # resolve markdown link destinations
```

Then, use relative links based off the directory of requesting page.

__Note:__ Another cause of error could be _repeated_ key definitions in Hugo's
site configuration file (`hugo.yaml`).

Any subsequent key definitions...
```
  markup:
    ...
  markup.goldmark:
    ...
  ```
will overwrite above `markup.goldmark.renderHooks.link` setting.

### Check embedding of site-local images

Page _2025-02-03-test_ (`index.md`) links to local image `./img0.png` and
global image `/assets/images/img0.png` with this directory layout:
```
my-project/
+--+ assets/
     +-- images/
         +-- img0.png
+--+ content/
     +-- posts/
         +-- 2025-02-03-test
             +-- img0.png
             +-- index.md
```
Method: Use relative links to \
\- `content/`-local images, based off the directory of requesting page, \
\- `assets/`-global images shared across pages with enabled image render hook.

#### Check against _works_ (+/-) that _img0_ link is rendered as embedded image:

resource | path | markdown link | rendered | works | need_hook | comment |
|:--|:--|:--|:--:|:--:|:--:|:--|
| local | relative | `![img0](img0.png)` | ![img0](img0.png) | + | - | __<-- BEST,__ image found locally |
| local | absolute | `![img0](/img0.png)` | ![img0](/img0.png) | + | - | same, stripped "/", image found locally |
| local | relative | `![img0](../../posts/2025-02-03-test/img0.png)` | ![img0](../../posts/2025-02-03-test/img0.png) | + | - | can navigate, content/ structure is preserved |
| local | relative | `![img0](../../../content/posts/2025-02-03-test/img0.png)` | ![img0](../../../content/posts/2025-02-03-test/img0.png) | - | - | content/ is a source dir, not a build dir |
| local | absolute | `![img0](/content/posts/2025-02-03-test/img0.png)` | ![img0](/content/posts/2025-02-03-test/img0.png) | - | - | content/ is a source dir, not a build dir |
| global | relative | `![img0](../../../assets/images/img0.png)` | ![img0](../../../assets/images/img0.png) | - | - | assets/ is a source dir, not a build dir |
| global | absolute | `![img0](/assets/images/img0.png)` | ![img0](/assets/images/img0.png) | - | - | assets/ is a source dir, not a build dir |
| global | relative | `![img0](images/img0.png)` | ![img0](images/img0.png) | + | + | __<-- BEST,__ image found globally |
| global | absolute | `![img0](/images/img0.png)` | ![img0](/images/img0.png) | + | + | same, stripped "/", found globally |
| global | relative | `![img0](../images/img0.png)` | ![img0](../images/img0.png) | + | + | can navigate, assets/ structure is preserved |

#### HowTo: Enable the _image render hook_

For Markdown _image links_ to work, enable the embedded
[image render hook](https://gohugo.io/render-hooks/images/)
in the site configuration (or provide custom hooks):
```
  markup:
    goldmark:
      renderHooks:
        image:
          enableDefault: true  # resolve markdown image destinations
```

__Note:__ Another cause of error could be _repeated_ key definitions in Hugo's
site configuration file (`hugo.yaml`).

Any subsequent key definitions...
```
  markup:
    ...
  markup.goldmark:
    ...
  ```
will overwrite above `markup.goldmark.renderHooks.image` setting.

__Note:__ The embedded shortcodes
[Ref](https://gohugo.io/shortcodes/ref/) and
[RelRef](https://gohugo.io/shortcodes/relref/)
only link to pages, not other resources: \
`` `![img0]({{%/* ref "img0.png" */%}})` `` is not a valid image link.

### Problem, 2025-02: frequent Hugo usage changes, unclear documentation

Hugo's development is still rapid, counting about ~300 minor and major
[release tags](https://github.com/gohugoio/hugo/tags)
over the last 8-years [2017..2024].

Just 2023/2024 saw various changes to Hugo's
[configuration options](https://gohugo.io/getting-started/configuration/),
[directory structure](https://gohugo.io/getting-started/directory-structure/),
and
[content management](https://gohugo.io/content-management/organization/).

For example, the `static/` directory has been replaced by `assets/` with a new
mechanism to "mount" directories of the former under the latter (for those
users that cannot migrate their directory structure).

Also, for links to local resources, there used to be an intuitive distinction
between \
\- _relative_ paths (`xyz`, `../xyz`, `../../posts/xyz`) with link destinations
  relative to the directory of the requesting page, \
\- _absolute_ paths (`/posts/xyz`, starting with "/"), which counted relative
  to the (build) site. \
This distinction seem to have been blurred, since a local image (part of the
same page bundle) is found under both `xyz.png` and `/xyz.png`.

A problem, which I could not easily resolve from the Hugo documentation: Some
_Markdown_ links with absolute paths `[...](/posts/...)` were lacking the
_subdirectory_ segment in the rendered URL: \
`http://localhost:1313/[missing URL segment]/posts/.../` \
Hugo's doc page strikes me as numerous and not always clear:
[cross-reference](https://gohugo.io/content-management/cross-references/),
[ref](https://gohugo.io/shortcodes/ref/),
[relref](https://gohugo.io/shortcodes/relref/),
[URL management](https://gohugo.io/content-management/urls/),
[link render hook](https://gohugo.io/render-hooks/links/),
[image render hook](https://gohugo.io/render-hooks/images/).

Of more help have been
[Joe Mooring](https://discourse.gohugo.io/u/jmooring/summary)'s
replies on the
[Hugo discourse](https://discourse.gohugo.io/),
for example, this
[forum issue](https://discourse.gohugo.io/t/52487),
whose code example helped getting the above tests work.

__Note:__ Important for links above to work to have
[relativeURLs](https://gohugo.io/content-management/urls/)
__disabled__ in the site configuration:
```
# replace absolute with relative URLs (to the current page) after rendering
relativeURLs: false  # default, "do not enable unless for a serverless site"
```
