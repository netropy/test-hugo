---
title: "4. Check rendering of LaTeX"
date: 2025-02-04
params:
  subtitle: Check LaTeX rendering via client- or server-side library
description: no description.
categories: ["web development", "blogging"]
tags: ["hugo", "latex"]
math: true  # recognized by some themes with preconfigured latex support
---

Summary: Prefer server-side rendering via Hugo's embedded KaTeX instance. \
Set up LaTeX processing in `passthrough:` and `partials/`.
<!--more-->

#### Compare the next section's 1st line against these screenshots:

If displayed as...

![no latex rendering](katex.missing_latex_rendering.png)
--> Problem: No client- or server-side rendering of LaTeX. \
=> See `passthrough` in file `hugo.yaml`, see `/layouts/_default/baseof.html`

![missing katex css](katex.missing_katex_css.png)
--> Problem: Server-side KaTeX rendering but not loading the `katex.min.css`
stylesheet. \
=> See `/layouts/_default/baseof.html`

![serverside katex with css](katex.serverside_katex_with_css.png)
--> Problem: Server-side stylesheet loaded but `\mathbb{N}` rendered
incorrectly. \
=> Check browser if `jsdelivr.net` script is blocked from running.

![serverside katex with css and jsdelivr js script](katex.serverside_katex_with_css_jsdelivr_js.png)
-> Correct: Server-side rendering with loaded stylesheet and running scripts.

#### Check more inline and block LaTeX expressions

Inline latex with default markers `\(..\)` has \(\sum_{i\in\mathbb{N}}a_ib_i\)
between text.

Inline latex with configured markers ``$`..`$`` has
$`\sum_{i\in\mathbb{N}}a_ib_i`$ between text.

Inline latex with alternative, theme-specific ``$..$`` has
$\sum_{i\in\mathbb{N}}a_ib_i$ between text.

__Problem, 2025-02: Some themes always render repeated `...$...$...` as
math.__ \
__Problem, 2025-02: Then, two-character delimiters ``$`...`$`` also don't
work.__

Latex block with `\[..\]`: must be a separate paragraph...

\[\sum_{i=1}^{n}a_ib_i\]
... otherwise, preceding text is suppressed.

Latex block with `$$..$$`: must be a separate paragraph...

$$\sum_{i=1}^{n}a_ib_i$$
... otherwise, preceding text is suppressed.

Check if single dollar sign `$1` is renders as currency: $1.

Check if two dollar signs `$1...$1` in the same paragraph is rendered as
currency or math: $1 some text $1.

Double dollar sign `$$` renders as $$, cannot be repeated in same line or
paragraph (even in backticks); otherwise, rendered as latex block.

### HowTos

#### Preconfigured LaTeX support enabled by `math: true` and `hugo.yaml`

Some Hugo themes (like
[Hextra](https://themes.gohugo.io/themes/hextra/)) come with preconfigured
support for rendering LaTeX expressions (also with the _mhchem_ package for
chemistry expressions, with diagramming packages etc).

Most likely, those themes use server-side rendering via Hugo's embedded
[KaTeX](https://katex.org/)
instance (the documentation does not always say).

Rendering of LaTeX is enabled _per page_ in its
[front matter](https://gohugo.io/content-management/front-matter/):
```
math: true  # recognized by some themes with preconfigured latex support
```

Also, the passthrough extension must be enabled in the `hugo.yaml` site
configuration:
```
markup:
  goldmark:
    extensions:
      passthrough:
        delimiters:
          block: [['\[', '\]'], ['$$', '$$']]
          inline: [['\(', '\)'], ['$`', '`$']]
        enable: true
```

#### Server-side rendered LaTeX via Hugo's embedded KaTeX instance:

See layout templates in this project's source repository or in
[test-hugo](https://gitlab.com/netropy/test-hugo):
```
/layouts/_default/_markup/render-passthrough.html
/layouts/partials/custom_head.html        best, if loaded by theme
/layouts/partials/extend_head.html        best, if loaded by theme
/layouts/_default/baseof.html             otherwise, if supported by theme
/layouts/partials/head.html               otherwise, within <head>..</head>
```

Also, the passthrough extension must be enabled in the `hugo.yaml` site
configuration, see previous section.

See resources: \
https://gohugo.io/functions/transform/tomath/ \
https://gohugo.io/render-hooks/passthrough/

#### Client-side rendered LaTeX via JS libs MathJax or KaTeX:

See layout templates in this project's source repository or in
[test-hugo](https://gitlab.com/netropy/test-hugo):
```
/layouts/partials/custom_head.html        best, if loaded by theme
/layouts/partials/extend_head.html        best, if loaded by theme
/layouts/_default/baseof.html             otherwise, if supported by theme
/layouts/partials/head.html               otherwise, within <head>..</head>
/layouts/partials/math.katex.html
/layouts/partials/math.mathjax.html
```

Also, the passthrough extension must be enabled in the `hugo.yaml` site
configuration, see previous section.  Also, above `partials` may require:
```
params.math: true  # read by client-side-rendered LaTeX if used
```

See resources: \
https://gohugo.io/content-management/mathematics/ \
https://pages.gitlab.io/hugo/posts/math-typesetting/ \
https://misha.brukman.net/blog/2022/04/writing-math-with-hugo/
