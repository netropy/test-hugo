# A manual mini test site for Hugo themes

The deployed website is here:
<https://netropy.gitlab.io/test-hugo> \
The website's source is here:
<https://gitlab.com/netropy/test-hugo>

__Background:__ \
[Hugo](https://gohugo.io)
is a popular
[static (web-) site generator](https://en.wikipedia.org/wiki/Static_site_generator)
(SSG). \
Hundreds of pre-designed
[themes](https://themes.gohugo.io/)
(style template packages) are available for Hugo. \
This mini test site can serve as a testbed, sanity check, or playground to
decide on Hugo themes. \
For more details, see:
[content/\_index.md](content/_index.md)

__No Hugo theme installed:__ \
This "themeless Hugo" site, while deployable, primarily serves as a test
template. \
It lacks all features added by themes: main menu, navigation buttons, fonts
and style sheets etc. \
For copies of this site with an installed hugo theme, see instances
[test-hugo-*](https://gitlab.com/users/netropy/projects).

### How to install a copy of this mini test site for a Hugo theme:

__Note: Manual installation & testing__ \
This installation procedure requires to manually merge a few theme template
files. \
Testing then consists of: navigate the deployed website, verify menus, and
check out its visual style.

Manual installations steps (good enough for an infrequent task):
1. Clone this "themeless Hugo"
   [test-hugo](https://gitlab.com/netropy/test-hugo) repository into directory
   `<test-hugo>`:
   ```
   ls test-hugo
       LICENSE          assets          hugo.yaml       public
       README.md        content         i18n            static
       archetypes       data            layouts         themes
   ```
1. Create a local Hugo repository for a Hugo theme `<test-hugo-xyz>`:
   ```
   hugo new site <test-hugo-xyz> --format yaml
   cd <test-hugo-xyz>
   ```
1. Install Hugo theme "xyz" as a git submodule into `themes/<xyz>`:
   ```
   git init
   git submodule add <git-url-of-xyz-theme> themes/<xyz>
   # alternatively, install as a Hugo module
   ```
1. Copy (or merge) project, gitlab, content files from `<test-hugo>`:
   ```
   cp -i <test-hugo>/{README.md,LICENSE,.gitignore,.gitlab-ci.yml} .
   cp -ir <test-hugo>/assets/* assets/
   cp -ir <test-hugo>/content/* content/
   ```
1. Copy (or merge) `shortcodes` from `<test-hugo>`:
   ```
   cp -ir <test-hugo>/layouts/shortcodes layouts/
   ```
1. Copy and merge `partials` from `<test-hugo>`:
   ```
   # client-rendered latex support: as needed
   mkdir -p layouts/partials
   cp -i <test-hugo>/layouts/partials/math.*.html layouts/partials/
   ```
   ```
   # latex support: as needed/available/supported by theme
   mkdir -p layouts/partials
   cp -i <test-hugo>/layouts/partials/extend_head.html layouts/partials/extend_head.html-
   cp -i themes/<xyz>/layouts/partials/extend_head.html layouts/partials/
   # merge demarcated sections: _extend_head.html- ~> extend_head.html_
   ```
1. Copy and merge the `_default` theme templates from `<test-hugo>`:
   ```
   # latex support: as needed/available/supported by theme
   mkdir -p layouts/_default
   cp -i <test-hugo>/layouts/_default/baseof.html layouts/_default/baseof.html-
   cp -i themes/<xyz>/layouts/_default/baseof.html layouts/_default/
   # merge demarcated sections: _baseof.html- ~> baseof.html_
   ```
   ```
   # server-rendered latex support: as needed
   mkdir -p layouts/_default/_markup
   cp -i <test-hugo>/layouts/_default/_markup/render-passthrough.html layouts/_default/_markup/
   ```
   ```
   # taxonomy support: as needed/available/supported by theme
   mkdir -p layouts/_default
   cp -i <test-hugo>/layouts/_default/taxonomy.html layouts/_default/taxonomy.html-
   cp -i themes/<xyz>/layouts/_default/taxonomy.html layouts/_default/
   # merge demarcated sections: _taxonomy.html- ~> taxonomy.html_
   ```
1. Copy (or merge) the Hugo site configuration from `<test-hugo>`:
   ```
   cp -i <test-hugo>/hugo.yaml .
   # then adapt settings: baseURL, theme, title, subtitle
   ```
1. Render the `<test-hugo-xyz>` site and open printed URL in browser:
   ```
   hugo serve
   # fallback, if pages are rendered incorrectly:
   rm -rf ./public; hugo serve
   ```
1. Check out: theme's visual style, site navigation, rendering of markdown
   etc.
