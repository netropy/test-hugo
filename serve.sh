#!/bin/sh

rm -rf public/
# --cleanDestinationDir  # remove files not found in static dirs
                         # set cleanDestinationDir in hugo.yaml
# --disableFastRender  # full rebuilds on change:
# --ignoreCache  # ignores the cache directory
# hugo serve --cleanDestinationDir --disableFastRender --ignoreCache
# hugo serve --ignoreCache
hugo serve
